<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 * @package App\Models
 * @property int $id
 * @property string $level
 * @property string $driver
 * @property string $message
 * @property string $trace
 * @property string $channel
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Log extends Model
{
    // TODO Implement
    use HasFactory;

    protected $table = 'logs';

    protected $fillable = [
        'level',
        'driver',
        'message',
        'trace',
        'channel'
    ];
}
