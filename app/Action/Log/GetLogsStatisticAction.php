<?php

namespace App\Action\Log;

use App\Repository\LogRepository;

class GetLogsStatisticAction
{
    public function __construct(private LogRepository $logRepository)
    {
    }

    public function execute(): GetLogsStatisticResponse
    {
        $response = $this->logRepository->getLogsStatistic();

        return new GetLogsStatisticResponse($response);
    }
}
