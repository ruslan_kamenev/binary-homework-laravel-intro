<?php

namespace App\Action\Log;

use Illuminate\Database\Eloquent\Collection;

class GetLogsByLevelResponse
{
    public function __construct(private Collection $collection)
    {
    }

    public function getLogs(): array
    {
        return $this->collection->toArray();
    }
}
