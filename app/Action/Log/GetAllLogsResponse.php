<?php

namespace App\Action\Log;


class GetAllLogsResponse
{
    public function __construct(private array $logs)
    {
    }

    public function getLogs(): array
    {
        return $this->logs;
    }
}
