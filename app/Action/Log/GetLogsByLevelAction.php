<?php

namespace App\Action\Log;

use App\Repository\LogRepository;

class GetLogsByLevelAction
{
    public function __construct(private LogRepository $logRepository)
    {
    }

    public function execute(string $level): GetLogsByLevelResponse
    {
        $response = $this->logRepository->findLogsByLevel($level);

        return new GetLogsByLevelResponse($response);
    }
}
