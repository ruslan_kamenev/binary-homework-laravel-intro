<?php

namespace App\Action\Log;

use App\Repository\LogRepository;

class GetAllLogsAction
{
    public function __construct(private LogRepository $logRepository)
    {
    }

    public function execute(): GetAllLogsResponse
    {
        $logs = $this->logRepository->findAll();

        return new GetAllLogsResponse($logs);
    }
}
