<?php

namespace App\Action\Log;

use App\Models\Log;
use Illuminate\Database\Eloquent\Collection;

class GetLogsStatisticResponse
{
    public function __construct(private Collection $statistic)
    {
    }

    public function getStatisticArray(): array
    {
        $statisticCollection = $this->statistic->map->only(['level', 'count']);
        $response = [];

        foreach ($statisticCollection as $key => $value) {
            $response[] = [$value['level'] => $value['count']];
        }

        return $response;
    }
}
