<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Collection;

interface LogRepositoryInterface
{
    public function findAll(): array;

    public function findLogsByLevel(string $level): Collection;

    public function getLogsStatistic(): Collection;
}
