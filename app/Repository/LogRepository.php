<?php

namespace App\Repository;

use App\Models\Log;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class LogRepository implements LogRepositoryInterface
{
    public function findAll(): array
    {
        return Log::all()->toArray();
    }

    public function findLogsByLevel(string $level): Collection
    {
        return Log::where('level', $level)->get();
    }

    public function getLogsStatistic(): Collection
    {
        return Log::groupBy('level')->select('level', DB::raw('count(*) as count'))->get();
    }
}
