<?php

namespace App\Http\Controllers;

use App\Action\Log\GetAllLogsAction;
use App\Action\Log\GetLogsByLevelAction;
use App\Action\Log\GetLogsStatisticAction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class LogController extends Controller
{
    public function getAllLogs(GetAllLogsAction $action): JsonResponse
    {
        $response = $action->execute();

        return response()->json($response->getLogs());
    }

    public function getLogsByLevel(GetLogsByLevelAction $action, string $level): JsonResponse|RedirectResponse
    {
        $availableLevels = ['info', 'warning', 'error', 'debug', 'critical', 'alert', 'emergency', 'notice'];

        if ($level === 'statistic') {
            return redirect()->route('logs-statistic');
        }

        if (in_array($level, $availableLevels)) {
            $response = $action->execute($level);

            return response()->json($response->getLogs());
        }

        return response()->json(['message' => 'Level not found.'], 404);
    }

    public function getLogsStatistic(GetLogsStatisticAction $action): View
    {
        $logs = $action->execute();

        return view('logs', ['logs' => $logs->getStatisticArray()]);
    }
}
