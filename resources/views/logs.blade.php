<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Log Viewer</title>
</head>
<body>
    Log Viewer
    <br>
    <table border="1px">
        <tr>
            <td>Level name</td>
            <td>Logs amount</td>
        </tr>
@foreach($logs as $log)
    <tr align="center">
        @foreach($log as $key => $value)
            <td>{{ $key }}</td><td>{{ $value }}</td>
        @endforeach
    </tr>
@endforeach
    </table>
</body>
</html>
