<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LogFactory extends Factory
{
    private array $levels = ['info', 'warning', 'error', 'debug', 'critical', 'alert', 'emergency', 'notice'];

    public function definition()
    {
        return [
            'level' => $this->levels[array_rand($this->levels)],
            'driver' => 'database',
            'message' => $this->faker->name,
            'trace' => $this->faker->text,
            'channel' => 'default'
        ];
    }
}
